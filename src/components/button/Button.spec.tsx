import React from 'react';
import { render } from '@testing-library/react';
import Button from './Button';

it('should render with the correct text', () => {
  const { getByTestId } = render(<Button text="Click" />);
  const rendered = getByTestId('button');
  expect(rendered).toBeTruthy();
});
