import React, {
  ButtonHTMLAttributes,
  DetailedHTMLProps,
  FC,
  ReactElement,
} from 'react';

export type ButtonProps = ButtonHTMLAttributes<HTMLButtonElement> & {
  text: string;
};

const Button: ReactElement<ButtonProps> = ({ text }: ButtonProps) => (
  <button
    className="bg-blue-700 text-white rounded-md px-4 py-2"
    data-testid="button"
    type="button"
  >
    {text}
  </button>
);

export default Button;
