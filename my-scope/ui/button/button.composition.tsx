import React from "react";
import { Button } from "./button";

export const BasicButton = () => (
  <Button color="tomato" text="hello from Button" />
);
