import React from "react";
import { render } from "@testing-library/react";
import { BasicButton } from "./button.composition";

it("should render with the correct text", () => {
  const { getByTestId } = render(<BasicButton />);
  const rendered = getByTestId("button");
  expect(rendered).toBeTruthy();
});
