import React from "react";

export type ButtonProps = {
  /**
   * a text to be rendered in the component.
   */
  text: string;
  /**
   * Color of the button text
   */
  color?: string;
};

export function Button({ color, text }: ButtonProps) {
  return (
    <button data-testid="button" style={{ color }}>
      {text}
    </button>
  );
}
